package bank.steps;

import cucumber.api.java.ru.Дано;
import cucumber.api.java.ru.Если;
import cucumber.api.java.ru.Тогда;
import org.junit.Assert;

public class ParametersStepdefs {

	@Дано("^Пользователь подумал вставить карту (\\d+) и ввел pin (\\d+)$")
	public void пользовательПодумалВставитьКартуИВвелPin(int arg1, int arg2) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Assert.assertTrue(arg1>-1);
		Assert.assertTrue(arg2>-1);
	}

	@Если("^Pin корректный \"([^\"]*)\"$")
	public void pinКорректный(Boolean arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Assert.assertTrue(arg1);
	}

	@Если("^Баланс счета достаточен (\\d+) для списания и в банкомате достаточно средств (\\d+)$")
	public void балансСчетаДостаточенДляСписанияИВБанкоматеДостаточноСредств(int arg1, int arg2) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Assert.assertTrue((0<arg1)&&(arg1<arg2));
	}

	@Тогда("^Пользователю выдается сумма$")
	public void пользователюВыдаетсяСумма() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

	}

	@Если("^Пользователь забрал деньги \"([^\"]*)\"$")
	public void пользовательЗабралДеньги(Boolean arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Assert.assertTrue(arg1);
	}

	@Тогда("^Сумма списывается с лицевого счета карты \"([^\"]*)\"$")
	public void суммаСписываетсяСЛицевогоСчетаКарты(Boolean arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Assert.assertTrue(arg1);
	}

	@Тогда("^Пользователю возвращается карта \"([^\"]*)\"$")
	public void пользователюВозвращаетсяКарта(Boolean arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Assert.assertTrue(arg1);
	}

	@Если("^Пользователь забрал карту \"([^\"]*)\"$")
	public void пользовательЗабралКарту(Boolean arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Assert.assertTrue(arg1);
	}

	@Тогда("^Операция завершена успешно$")
	public void операцияЗавершенаУспешно() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

	}

}
