# language: ru

@all
Функционал: Передача параметров/наборов параметров


  @correct
  Структура сценария: Успешная аутентификация
    Дано Пользователь подумал вставить карту <cardIn> и ввел pin <pinIn>
    Если Pin корректный "<pinCorrect>"
    И Баланс счета достаточен <balance> для списания и в банкомате достаточно средств <moneyInBank>
    Тогда Пользователю выдается сумма
    Если Пользователь забрал деньги "<take>"
    Тогда Сумма списывается с лицевого счета карты "<amount>"
    Также Пользователю возвращается карта "<retCard>"
    Если Пользователь забрал карту "<takeCard>"
    Тогда Операция завершена успешно
    Примеры:
      | cardIn | pinIn | pinCorrect | balance | moneyInBank | take | amount | retCard | takeCard |
      | 777    | 1234  | true       | 1000    | 100000      | true | true   | true    | true     |
      | 333    | 0000  | true       | 100     | 100000      | true | true   | true    | true     |
      | 000    | 3333  | true       | 1       | 100000      | true | true   | true    | true     |
      | 777    | 1234  | true       | 1000    | 100000      | true | true   | true    | true     |
      | 333    | 0000  | true       | 100     | 100000      | true | true   | true    | true     |
      | 000    | 3333  | true       | 1       | 100000      | true | true   | true    | true     |
      | 222    | 2222  | true       | 200000  | 100000      | true | true   | true    | true     |

