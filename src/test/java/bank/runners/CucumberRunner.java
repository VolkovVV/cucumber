package bank.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(tags = {"@all"},
		format = {"pretty", "json:target/cucumber.json","html:target/cucumber.html"},
		features = {"src/test/java/bank/features"}
		,glue = "bank.steps"
		,snippets = SnippetType.CAMELCASE

		)
public class CucumberRunner {
}
